

#include <iostream>
#include <time.h>


int main()
{
    const int N = 5;
    int numbers[N][N];

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            numbers[i][j] = i + j;
            std::cout << numbers[i][j] << " ";
        }
        std::cout << "\n";
    }
    
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);
    int currentDay = buf.tm_mday;
    int row = currentDay % N;
    int sum = 0;

    for (int j = 0; j < N; j++)
    {
        sum += numbers[row][j];
    }

    std::cout << "Sum in the row " << row + 1 << ": " << sum << "\n";
 
}


